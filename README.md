Arduino Project: Magnetometer
=============================


Project setup
-------------

To make a Raspberry Pi ready for the project, follow the following steps

1. Install Raspbian(full)
2. Run setup.sh on the Raspberry Pi with sudo rights

or simply clone an ssd which is already ready to use.

The Raspberry Pi should now have

- a working Arduino toolbox
- the necessary Arduino libraries for the project 
- a directory on the desktop with all files needed for the project
- a serialmonitor executable on the desktop


Serial Monitor
--------------

serialmonitor.py is a small python application which can be used to monitor
the data send over USB by the arduino.


Notes:
------

* To keep things simple, let's use the same username and password on
  every Raspberry Pi (username: "pi", password: "pluto")

* The time and date on the Raspberry Pi might not be correct because
  the UGent blocks ntp traffic. Use the following command to update
  the time on the Raspberry Pi

        $ sudo ntpdate ntp.UGent.be

* The file ipAddresses.txt contains a list with the ip addresses of the
  Raspberry Pies. If the Raspberry Pies are set correctly, you should
  be able to ssh into the Raspberry Pies.