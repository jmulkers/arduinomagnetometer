#!/bin/bash

# you need sudo rights to run this script
if [ "$EUID" -ne 0 ]
then 
	echo "Run this script as root"
fi

# make sure ssh is enabled (0 is enable for some weird reason)
sudo raspi-config nonint do_ssh 0

# set the correct date and time
#   (still need to find a way to set the correct date and time 
#    every time the raspberry is rebooted)
apt-get -y install ntpdate
ntpdate ntp.UGent.be

# install some tools I like and which might be useful for students or to help students
apt-get -y install vim gnuplot ipython3

# download and install arduino toolbox
wget "https://www.arduino.cc/download.php?f=arduino-1.8.10-linuxarm.tar.xz" -O /tmp/arduino.tar.xz
tar -xvf /tmp/arduino.tar.xz -C /opt
chmod -R a+rX /opt/arduino-1.8.10 # make it readable and executable for all
/opt/arduino-1.8.10/install.sh

# install additional arduino libraries for the pi user
runuser -l pi -c 'arduino --install-library "Adafruit MLX90393"'
runuser -l pi -c 'arduino --install-library "Bifrost library for HC-SR04"'
# fix the error in the HC-SR04 library ('#include "HCSR04.h"' -> '#include "hcsr04.h"')
sed -i -e 's/HCSR04.h/hcsr04.h/g' /home/pi/Arduino/libraries/Bifrost_library_for_HC-SR04/src/hcsr04.cpp 


# Copy the ArduinoSketches directory and serialmonitor.py to the desktop of the pi user
setupDir=$(dirname "$(readlink -f "$0")")
cp -r $setupDir/ArduinoSketches /home/pi/Desktop
cp $setupDir/serialmonitor.py /home/pi/Desktop/serialmonitor

# Download the documentation of the sensors
DOCDIR=/home/pi/Desktop/ArduinoSketches/Documentation
mkdir -p $DOCDIR
wget "https://www.melexis.com/-/media/files/documents/datasheets/mlx90393-datasheet-melexis.pdf" -O $DOCDIR/mlx90393-datasheet.pdf
wget "https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf" -O $DOCDIR/HCSR04.pdf