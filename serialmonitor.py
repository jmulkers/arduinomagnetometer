#!/usr/bin/env python3
import numpy as np
import tkinter as tk
from tkinter import filedialog
import serial
import glob

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

BAUDCOUNT = 9600
SAMPLERATE = 20 #ms
BUFSIZE = 100

class SerialRecorder(tk.Frame):

    def __init__(self, master):
        self.master = master
        self.master.geometry("500x800")
        self.master.minsize(400,700)
        super().__init__(self.master)
        self.master.title("Serial Monitor")

        self.recordButtonText = tk.StringVar()
        self.recordButtonText.set("Start recording")
        self.recordButton = tk.Button(self, 
            textvariable=self.recordButtonText, 
            command=self.record_button_callback)

        self.plotColumnSettings = tk.Frame(self)
        self.plotColumn = tk.Variable(self)
        self.plotColumn.set("None")
        self.plotColumnLabel = tk.Label(self.plotColumnSettings, text="Plot column")
        self.plotColumnMenu = tk.OptionMenu(
            self.plotColumnSettings, 
            self.plotColumn, 
            *["None"] + list(range(10)), 
            command=lambda v: self.reset_buffer())
        self.plotColumnLabel.pack(side=tk.LEFT)
        self.plotColumnMenu.pack(side=tk.LEFT)

        self.log = tk.Text(self)
        self.log.config(state=tk.DISABLED)

        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)

        # place the widgets
        self.recordButton.grid(column=0, row=0, sticky=tk.W, padx=4, pady=5)
        self.log.grid(column=0, row=1, sticky=tk.W+tk.E, padx=10, pady=4)
        self.plotColumnSettings.grid(column=0,row=2, sticky=tk.W)
        self.canvas.get_tk_widget().grid(column=0, row=3, sticky=tk.W+tk.E+tk.N+tk.S, padx=10, pady=5)
        self.pack(side="bottom", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=0)
        self.grid_rowconfigure(1, weight=0)
        self.grid_rowconfigure(3, weight=2)
        self.grid_columnconfigure(0, weight=1)

        self.recording = False
        self.recordedlines = []

        self.reset_buffer()
        self.start()

    def stop(self):
        plt.close(self.fig)
        self.master.destroy()

    def reset_buffer(self):
        self.clear_plot()
        self.databuffer = np.zeros(BUFSIZE) + np.nan # fill buffer with nans

    def start(self):
        ACMcontrollers = glob.glob("/dev/ttyACM?")
        if len(ACMcontrollers)==0:
            self.print_line("# No arduino found. Check your connections\n")
            self.after(1000, self.start) # try again after a second
            return

        try:
            # TODO: we take the first ACM controller, but we should let the user decide
            self.arduinoSerial = serial.Serial(ACMcontrollers[0], BAUDCOUNT)
        except: # something went wrong, try again
            self.after(1000, self.start) # try again after a second
            return
        else: # We have a connection, let's take samples
            self.sample()

    def record_button_callback(self):
        if self.recording: # stop recording
            self.recording = False
            self.save_recorded_lines()
            self.recordButtonText.set("Start recording")
        else: # start recording
            self.recordedlines = []
            self.recording = True
            self.recordButtonText.set("Stop recording and save")

    def sample(self):
        if self.winfo_exists() == 0:
            return
        try:
            dataWaiting = self.arduinoSerial.in_waiting
        except: # something went wrong, try to reconnect
            self.start()
            return

        if dataWaiting:
            line = self.arduinoSerial.readline()
            self.print_line(line)
            self.update_plot(line)
            if self.recording:
                self.recordedlines.append(line)

        if self.winfo_exists() == 0:
            return

        self.after(SAMPLERATE, self.sample)

    def print_line(self,line):
        self.log.config(state=tk.NORMAL)
        self.log.insert(tk.END, line)
        self.log.config(state=tk.DISABLED)
        self.log.see(tk.END) # scroll to the end

    def save_recorded_lines(self):
        file = filedialog.asksaveasfile(mode='wb', defaultextension=".txt")
        if file:
            file.writelines(self.recordedlines)
            file.close()

    def clear_plot(self):
        self.ax.clear()
        self.ax.set_xlim([0,BUFSIZE])
        self.canvas.draw()

    def update_plot(self, line):
        if line[0] == "#":
            return

        try:
            value = float(line.split()[self.plotColumn.get()])
        except:
            return

        self.databuffer = np.roll(self.databuffer, -1)
        self.databuffer[-1] = value

        self.ax.clear()
        self.ax.set_xlim([0,BUFSIZE])
        self.ax.plot(self.databuffer, 'k-o', lw=1, ms=2, c="gray")
        self.canvas.draw()




if __name__ == "__main__":
    root = tk.Tk()
    app=SerialRecorder(master=root)
    app.master.protocol("WM_DELETE_WINDOW", app.stop)
    app.mainloop()