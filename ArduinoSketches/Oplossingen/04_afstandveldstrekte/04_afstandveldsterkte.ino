/*
 * Opdracht:
 * 
 *  a. Sluit zowel de magnetometer als de afstandsmeter aan op de Arduino.
 * 
 *  b. Schreef hieronder een Arduino sketch zodat op een vast tijdsinterval
 *     (bv 500ms) de afstandsmeter en de magnetometer uitleest en verstuurd
 *     over een USB connectie.
 * 
 *  c. Gebruik deze opstelling om de sterkte van het magnetische veld  van
 *     een magneet te bepalen in functie van de afstand tussen de magneet
 *     en de sensor
 * 
 */


/*************** OPLOSSING ******************/

#include "Adafruit_MLX90393.h"  
#include "hcsr04.h"

HCSR04 rangeSensor(12,13);
Adafruit_MLX90393 magnetometer;

void setup() {
    Serial.begin(9600);
    magnetometer.begin();
}

void loop() {
    int distance = rangeSensor.distanceInMillimeters();
    float Bx, By, Bz;
    magnetometer.readData(&Bx, &By, &Bz);
    Serial.print(distance);
    Serial.print("\t");
    Serial.print(Bx);
    Serial.print("\t");
    Serial.print(By);
    Serial.print("\t");
    Serial.print(Bz);
    Serial.print("\n");
    delay(200);
}

/********************************************/