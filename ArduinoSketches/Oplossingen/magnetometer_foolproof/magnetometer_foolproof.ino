#include "Adafruit_MLX90393.h"  /* needed to control the mlx90393 magnetometer */
#include "hcsr04.h"  /* needed to control the hc-sr04 ultrasonic ranging sensor */


/* At every SAMPLE_RATE ms, we measure the distance and magnetic field */
#define SAMPLE_RATE 200


/* Construct HCSR04 instance to facilitate the communication with hc-sr04*/
#define TRIG_PIN 12    /* arduino pin connected to the trigger of the sensor */
#define ECHO_PIN 13    /* arduino pin connected to the echo pin of the sensor */
#define MIN_DIST 20    /* minimum distance in mm */
#define MAX_DIST 600   /* maximum distance in mm */
HCSR04 rangeSensor(TRIG_PIN, ECHO_PIN, MIN_DIST, MAX_DIST);


/* Construct Adafruit_MLX90393 instance to facilitate the communication with mlx90393 */
Adafruit_MLX90393 magnetometer;


void setup(void) {
  
  /* Initialize a USB connection with Baud rate = 9600 */
  Serial.begin(9600);

  /* Wait for the USB connection */
  while (!Serial) {
    delay(10);
  }

  /* Look for the magnetometer */
  Serial.println("# Looking for the MLX90393 magnetometer");
  while (!magnetometer.begin()) {
    Serial.println("# Magnetometer not found... check your wiring?");
    delay(1000);
  }
  Serial.println("# Found the MLX90393 magnetometer");
}


void loop(void) {

  /* Read the magnetometer and put the values in Bx, By, and Bz */
  float Bx, By, Bz;
  bool succes = magnetometer.readData(&Bx, &By, &Bz);

  if (succes) {
    /* measure the distance */
    int distance = rangeSensor.distanceInMillimeters();

    /* Print the distance, Bx, By, and Bz, seperated by tabs */
    Serial.print(distance); // Print distance
    Serial.print("\t");     // Tab
    Serial.print(Bx, 2);    // Print x-component of magnetic field in uT
    Serial.print("\t");     // Tab
    Serial.print(By, 2);    // Print y-component of magnetic field in uT
    Serial.print("\t");     // Tab
    Serial.print(Bz, 2);    // Print z-component of magnetic field in uT
    Serial.print("\n");     // New line

  } else { 
    Serial.println("# Unable to read data from the MLX90393 sensor.");
  }

  /* Wait for SAMPLE_RATE ms before taking a new sample */
  delay(SAMPLE_RATE);  
}