/*
 * Opdracht:
 *
 *  a. Verbind de trigger pin en de echo pin van de hcsr04 afstandsmeter
 *     met respectievelijk pins 12 en 13 van de Arduino.
 *
 *  b. Implementeer de setup en loop functie zodat de Arduino elke seconde 
 *     de afstand opmeet en deze doorstuurt over de USB connectie.
 *
 */



/* Hier laden we reeds bestaande code in dat ons zal helpen met het 
 * communiceren met de hcsr04. Deze code kan je terug vinden in 
 * /home/pi/Arduino/libraries
 */
#include"hcsr04.h"


/* Maak een HCSR04 instantie aan (genaamd rangeSensor) dat zal communiceren 
 * met de afstandsmeter. Hierbij moet aangegeven worden op welke pins de 
 * afstandsmeter aangesloten zijn op de Arduino.
 *
 * Deze HCSR04 instantie kan vervolgens gebruikt worden om afstand op te meten:
 *
 *      int distance = rangeSensor.distanceInMillimeters();
 */
HCSR04 rangeSensor( 12 , 13 );


void setup() {
    /*************** OPLOSSING ******************/
    Serial.begin(9600);
    /********************************************/
}


void loop() {
    /*************** OPLOSSING ******************/
    int distance = rangeSensor.distanceInMillimeters();
    Serial.print(distance);
    Serial.print("\n");
    delay(1000);
    /********************************************/
}
