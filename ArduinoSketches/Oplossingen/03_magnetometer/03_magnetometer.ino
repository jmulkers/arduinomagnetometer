/* 
 * Opdracht
 * 
 *  a. Verbind de clock pin (SCL) en de data pin (SCA) van de Adafruit_MLX90393
 *     magnetometer met de clock en data pin van de Arduino. Geef de sensor
 *     een controle spanning van 3.3V (dus geen 5V!)
 * 
 *  b. Implementeer de setup en loop functie zodanig dat de arduino elke 
 *     seconde het magnetische veld uitleest en verzend over de USB connectie
 * 
 *  c. Bestudeer hoe de sterkte van het magnetische veld verandert als je de
 *     opstelling 45 graden draait in afwezigheid van een magneet
 * 
 *  d. Bestudeer hoe de sterkte van het magnetische veld verandert als je een
 *     magneet in nabijheid brengt van de magnetische sensor
 * 
 */



/* Ook hier maken we gebruik van reeds bestaande code om te communiceren met
 * de magnetometer
 */
#include "Adafruit_MLX90393.h"  


/* Hier maken van een Adafruit_MLX90393 instantie aan (genaamd "magnetometer")
 * dat zal communiceren via het I2C protocol met de mlx90393 magnetometer.
 * 
 * De I2C connectie met de magnetometer kan geinitialiseerd worden in de
 * setup functie met:
 * 
 *   magnetometer.begin();
 * 
 * De sterkte van het magnetische veld (Bx,By,Bz) kan uitgelezen worden met:
 *
 *   float Bx, By, Bz;
 *   magnetometer.readData(&Bx, &By, &Bz);
 */
Adafruit_MLX90393 magnetometer;


void setup() {
    /*************** OPLOSSING ******************/
    Serial.begin(9600);
    magnetometer.begin();
    /********************************************/
}


void loop() {
    /*************** OPLOSSING ******************/
    float Bx, By, Bz;
    magnetometer.readData(&Bx, &By, &Bz);
    delay(1000);
    Serial.print(Bx);
    Serial.print("\t");
    Serial.print(By);
    Serial.print("\t");
    Serial.print(Bz);
    Serial.print("\n");
    /********************************************/
}