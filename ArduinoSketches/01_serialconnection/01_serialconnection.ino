/* 
 * Opdracht:
 * 
 *  Implementeer de loop functie zodat de arduino na elke seconde de counter
 *  met 1 vermeerderd en de waarde van de counter verstuurd over de USB
 *  connectie.
 * 
 *  Tip: je kan de delay functie gebruiken om de Arduino een welbepaalde tijd
 *  te laten wachten alvorens verder te gaan:
 * 
 *       delay(1000); // wacht voor 1000ms
 */


/* De variabele die we als counter zullen gebruiken */
int counter;


/* De setup functie wordt enkel uitgevoerd bij het aanzetten of resetten van de Arduino */
void setup() {

    /* Initialiseer een USB connectie met een 9600 baud rate */
    Serial.begin(9600);


    /* Verstuur een string met een 'newline character' op het einde */
    Serial.print( "We hebben een connectie!\n" );


    /* reset de counter en verstuur de waarde over de USB connectie */
    counter = 0;
    Serial.print( "Reset the counter: counter = " );
    Serial.print( counter );
    Serial.print( "\n" );
}


/* Na de setup functie wordt de loop functie herhaaldelijk uitgevoerd */
void loop() {

}