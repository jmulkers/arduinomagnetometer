/*
 * Opdracht:
 * 
 *  1. Sluit zowel de magnetometer als de afstandsmeter aan op de Arduino.
 * 
 *  2. Schreef hieronder een Arduino sketch zodat op een vast tijdsinterval
 *     (bv 500ms) de afstandsmeter en de magnetometer uitgelezen wordt en
 *     de resultaten worden verstuurd over een USB connectie.
 * 
 *  3. Gebruik deze opstelling om de sterkte van het magnetische veld van
 *     een magneet te bepalen in functie van de afstand tussen de magneet
 *     en de sensor. Het programmatje /home/pi/Desktop/serialmonitor is
 *     een python programmatje dat net zoals de serial monitor in de 
 *     Arduino weromgeving gebruikt kan worden om de inkomende data te
 *     monitoren. Bovendien laat dit programmatje toe om de data weg te 
 *     schrijven naar een data bestand. Dit bestand kan je dan met een
 *     spreadsheet programma inlezen om vervolgens je resultaten in een
 *     grafiek te plaatsen.

 */